import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { RegisterService } from '../../services/http';

@Component({
  templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  // phonePattern = "^((\\+91-?)|0)?[0-9]{10}$";
  phonePattern = "^\\(?(0|\\+61)[24378]\\)?[ -]?[0-9]{2}[ -]?[0-9]{2}[ -]?[0-9][ -]?[0-9]{3}$";


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private registerService: RegisterService,
  ) { }

  public ngOnInit() {
    this.registerForm = this.formBuilder.group({
      company: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  public onSubmit() {
    console.log(this.f);
    this.submitted = true;
    // validate forms
    if(this.registerForm.invalid) {
      return;
    }
    
    this.loading = true;
    // registration http post implementation
    this.registerService.register(this.f.company.value, this.f.username.value, this.f.email.value, this.f.phone.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('data: +', data); 
          if(data.result.resultDetail == 'Created') {
            // navigate to login page
            this.router.navigate(['/login']);
          }
        }, error => {
          console.log('error: +', error);
          // show alert message here if error =="Bad Request"
          this.loading = false;
          this.router.navigate(['/login']);
        }
      );
  }

}

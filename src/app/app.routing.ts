import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './services/guards';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';

export const routes: Routes = [
  { path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: '',
    component: FullLayoutComponent,
    data: { title: 'Home' },
    canActivate: [AuthGuard],
    children: [{ path: 'dashboard', loadChildren: './views/dashboard/dashboard.module#DashboardModule' }]
  },
  { path: '',
    component: SimpleLayoutComponent,
    children: [{ path: 'login', loadChildren: './views/login/login.module#LoginModule'},
              { path: 'register', loadChildren: './views/register/register.module#RegisterModule'}],
  },

];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {enableTracing: false}) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
